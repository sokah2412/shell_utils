#!/bin/sh

[ "$#" -ne 0 ] && name="$1" || name="main"
name=$(cut -d'.' -f1 <<< $name)

#[ -f Makefile ] ||
#{
  echo "SRC = $name" > Makefile
  cat ~/afs/Program/bin/src/Makefile >> Makefile
#}

cp ~/afs/Program/bin/src/template_main.c "$name.c"

exit 1
