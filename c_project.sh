#!/bin/sh

NM=0
SRC="main.c"
INCLUDE=""
INCLUDE_C=""

[ "$#" -lt 1 ] && exit 1

while [ True ]; do
  if [ "$1" = "-nm" -o "$1" = "--no-main" ]; then
    shift
    NM=1
    continue
  fi
  if [ "$1" = "-i" -o "$1" = "--include" ]; then
    if ! [ -f "$2" ]; then
      echo "No file $2 to include"
      [ "$NM" -eq 1 ] ||
        rm main.c
      exit 1
    fi
    while read line; do
      INCLUDE="$INCLUDE$line\n"
    done < "$2"
    INCLUDE_C="$INCLUDE"
    shift 2
    continue
  fi
  if [ "$1" = "-ic" -o "$1" = "--include-c" ]; then
    echo "test"
    if ! [ -f "$2" ]; then
      echo "No file $2 to include"
      [ "$NM" -eq 1 ] ||
        rm main.c
      exit 1
    fi
    while read line; do
      INCLUDE_C="$INCLUDE_C$line\n"
    done < "$2"
    shift 2
    continue
  fi  
  if [ "$1" = "-im" -o "$1" = "--include-main" ]; then
    if ! [ -f "$2" ]; then
      echo "No file $2 to include"
      [ "$NM" -eq 1 ] ||
        rm main.c
      exit 1
    fi
    while read line; do
      INCLUDE="$INCLUDE$line\n"
    done < "$2"
    shift 2
    continue
  fi
  break
done

if [ "$NM" -eq 0 ]; then
  if [ -f main.c ]; then
      echo "main.c already exists in this directory."
      exit 1
    fi
    cp ~/afs/Program/bin/src/template_main.c main.c
fi
for args; do
  case "$args" in
    *.*)
      name=$(cut -d'.' -f1 <<< "$args")
      ext=$(cut -d'.' -f2 <<< "$args")
      if [ "$ext" = "c" ]; then
        [ -f "$args" ] && {
          echo "$args already exit, file add to Makefile"
        } || {
          echo -e "$INCLUDE_C#include \"$name.h\"" > "$args"
          [ -f "$name.h" ] && {
            echo "$args already exit"
          } || {
            cp ~/afs/Program/bin/src/template_h.h "$name.h"
            name_garde=$(tr 'a-z.\-#&' 'A-Z_' <<< "$args")
            sed -i "s/_H/$name_garde/g" "$name.h"
          }
          INCLUDE="$INCLUDE#include \"$name.h\"\n"
        }
        SRC="$SRC $args"
      elif [ "$ext" = "h" ]; then
        [ -f "$args" ] && {
          echo "$args already exit"
        } || {
          cp ~/afs/Program/bin/src/template_h.h "$args"
          name_garde=$(tr 'a-z.\-#&' 'A-Z_' <<< "$args")
          sed -i "s/_H/$name_garde/g" "$args"
        }
        INCLUDE="$INCLUDE#include \"$args\"\n"
      else
        echo "$args has an extension not recognize : $ext : Skipped"
        continue
      fi
      ;;
    *)
      echo "$args doesn't have an extension: Skipped"
      continue
      ;;
  esac
done

[ "$NM" -eq 1 ] ||
{
  sed -i "s/\(\[INCLUDE_H\]\)/$INCLUDE/" main.c
}

[ -f Makefile ] &&
{
  ans="P"
  while [ "$ans" = "P" ]; do
    echo "Do you want to overwrite your Makefile ? [Y/N/P]"
    read ans
    if [ "$ans" = P ]; then
      cat Makefile
    elif [ "$ans" = "Y" ]; then
      echo "Makefile Overwrite"
      continue;
    elif [ "$ans" = "N" ]; then
      exit 0
    else
      echo "Please enter Y(es) or N(o) or P(rint Makefile)"
      ans="P"
    fi
  done
}

echo "SRC=$SRC" > Makefile
cat ~/afs/Program/bin/src/Makefile_m >> Makefile
