#!/bin/sh

ARGS=
[ $# -ne 0 ] && ARGS="$@"

NEW='\\e[92m'
EDIT='\\e[94m'
DEL='\\e[91m'
MERGE='\\e[31m'
TXT='\\e[37m'
RESET='\\e[0m'
YELLOW='\\e[93m'

gitlog=$(git log $ARGS --color=always --format=format:"%C(yellow)%h%C(reset) %s %C(blue)(%an)%C(reset)NEWLINE%C(auto)S%d")
s_rmNL=$(echo "$gitlog" | sed -e 's/NEWLINE[^S]*S$//')
s_clNL=$(echo "$s_rmNL" | sed -e "s/NEWLINE[^S]*S\(.*\)/\n     └─\1/")
s_edit=$(echo "$s_clNL" | sed -e "s/\[EDIT\]\([^]]*]\)/$EDIT[EDIT]$TXT\1$RESET/")
s_del=$(echo "$s_edit" | sed -e "s/\[DEL\]\([^]]*]\)/$DEL[DEL]$TXT\1$RESET/")
s_new=$(echo "$s_del" | sed -e "s/\[NEW\]\([^]]*]\)/$NEW[NEW]$TXT\1$RESET/")
s_merge=$(echo "$s_new" | sed -e "s/\[MERGE\]\([^]]*]\)/$MERGE[MERGE]$TXT\1$RESET/")
s_end=$(echo "$s_merge" | sed -e "$ s/$/\n/")

echo -e "$s_end"
