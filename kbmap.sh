#!/bin/sh

[ "$(setxkbmap -query | grep 'variant')" = "" ] && setxkbmap us -variant intl || setxkbmap us
xmodmap ~/.Xmodmap
