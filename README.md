# shell_utils

This a repository where I push my little shell script.
If you think it's interesting, you can check and use/modify them as you want.
If you have any questions or tips, you can contact me at antoine@hacquard.xyz.

## Precision for pretty_gitlog.sh :

This script recolor your git log for when you use a particular style to do your
commit messages:

```
[ACTION][FILE] Message
```

Four actions are available :
 - NEW
 - EDIT
 - DEL
 - MERGE

You can put anything for file, just some little tips :
 - Commit one feature by one, so you can easily put the file edit here
 - If you merge to branch, just indicate the 2 branches you have merge
   (ex : [MERGE][branch1/branch2] Merge branch1 with branch2)

Update:
 - You can now add options to pretty_gitlog.sh
    (Ex : pretty_gitlog.sh --all --graph)

That's it! Feel free to change color or add new actions~
(Yeah I know, I'm not the very best at sed but it works ;) )
